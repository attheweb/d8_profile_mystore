<?php



$aliases['dev'] = array(
  'root' => '/Users/jensdegeyter/Webserver/projects_test/d8_commerce_demo/build/html/',
  'uri' => 'www.mystore.dev',
  'path-aliases' => array( '%files' => 'sites/default/files')
);

$aliases['staging'] = array(
  'root' => '/data/sites/web/drupalgent/subsites/mystore.demo.drupal.gent/web/',
  'uri' => 'mystore.demo.drupal.gent',
  'remote-host' => 'ssh.drupal.gent',
  'remote-user' => 'drupalgent',
  'db-url' => 'mysql://ID179002_dmystore:ID179002_dmystore@ID179002_dmystore.db.webhosting.be/ID179002_dmystore',
  'path-aliases' => array( '%files' => 'sites/default/files')
);